package com.project.optimisticlock;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;

import org.hamcrest.Matchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.project.optimisticlock.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

// inspiration sources:
//https://spring.io/guides/gs/testing-web/
//https://thepracticaldeveloper.com/guide-spring-boot-controller-tests/

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TestAccountController {
    private final String POST_ACCOUNT_JSON_1 = "{\n" +
            "    \"name\": \"Account2\",\n" +
            "    \"balance\": 0\n" +
            "}";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountService accountService;

    @Test
    public void contextLoads() {
    	Assertions.assertNotNull(accountService);
    }

    @Test
    public void shouldReturnEmptyListWhenNoAccountCreated() throws Exception {
    	this.mockMvc.perform(get("/account"))
    		.andDo(print()).andExpect(status().isOk())
    		.andExpect(content().string(Matchers.containsString("[]")));
    }
    
    @Test
    public void shouldBeAbleToCreateNewAccount() throws Exception {
    	this.mockMvc.perform(post("/account").contentType(MediaType.APPLICATION_JSON).content(POST_ACCOUNT_JSON_1))
    		.andDo(print()).andExpect(status().isOk());
    }
    
    //it just doesn't work
    @Test
    public void shouldHandleMultipleParallelUpdateRequests() throws Exception{
    	Assertions.assertNotNull(accountService);
    	this.mockMvc.perform(post("/account").contentType(MediaType.APPLICATION_JSON).content(POST_ACCOUNT_JSON_1));
    	MockMvc mockMvc = this.mockMvc;
    	ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
    	for (int i = 0; i < 10; i++) {
    		final int amount = i;
			executor.execute((Runnable)()->{
				try {
					mockMvc.perform(get("/account/1/increment/"+amount))
						.andDo(print())
						.andExpect(status().isOk());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		}
	}
}
