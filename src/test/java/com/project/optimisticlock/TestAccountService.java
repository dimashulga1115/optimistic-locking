package com.project.optimisticlock;

import com.project.optimisticlock.account.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyLong;


@RunWith(MockitoJUnitRunner.class)
public class TestAccountService {

    @Mock
    AccountRepository accountRepository;
    AccountService accountService;

    @Before
    public void init(){
        AccountEntity entity = new AccountEntity(1L, "Account1", 0L);
        when(accountRepository.findById(anyLong())).thenReturn(Optional.of(entity));
        accountService = new AccountServiceImpl(accountRepository);
    }

    @Test
    public void shouldHandleHighLoad(){
        accountService.createAccount(new AccountDto(null,"Account1", 0L));
        for (int i = 1; i < 1000; i++) {
            accountService.addAmount(1L,i);
        }
        Assert.assertEquals((Long) 499_500L, accountService.getById(1L).getBalance());
    }

    @Test
    public void shouldHandleIncome(){}

    @Test
    public void shouldHandleCharge(){}

    @Test
    public void shouldHandleConcurrentAccess(){}
}
