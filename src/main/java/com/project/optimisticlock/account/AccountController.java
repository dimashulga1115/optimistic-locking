package com.project.optimisticlock.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/account")
public class AccountController {

    AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public List<AccountDto> getBalance(String accountName) {
        return accountService.getAllAccounts();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public AccountDto createAccount(@RequestBody AccountDto account){
        return accountService.createAccount(account);
    }

    @GetMapping(path = "/{id}/increment/{amount}")
    public void incrementBalance(@PathVariable(name = "id") Long accountId, @PathVariable Integer amount) {
        accountService.addAmount(accountId, amount);
    }

    @GetMapping(path = "/{id}/decrement/{amount}")
    public void decrementBalance(@PathVariable(name = "id") Long accountId, @PathVariable Integer amount) {
        accountService.substractAmount(accountId, amount);
    }
}
