package com.project.optimisticlock.account;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Transactional
    @Override
    public AccountDto getById(Long id) {
        return AccountConverter.convert(accountRepository.findById(id).get());
    }

    @Transactional
    @Override
    public List<AccountDto> getAllAccounts() {
        return accountRepository.findAll().stream()
                .map(accountEntity -> AccountConverter.convert(accountEntity))
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public AccountDto createAccount(AccountDto account) {
        Assert.notNull(account, "Account cannot be null.");
        Assert.isNull(account.getId(), "Account ID must be null for creation.");
        AccountEntity entity = AccountConverter.convert(account);
        entity = accountRepository.save(entity);
        return AccountConverter.convert(entity);
    }

    @Transactional
    @SneakyThrows
    @Override
    public Optional<AccountDto> addAmount(Long id, Integer amount) {
        if(amount < 0) throw new IllegalArgumentException("Amount cannot be negative");
        Optional<AccountDto> account;
        try {
            AccountEntity entity = accountRepository.getById(id);
            if(entity == null) throw new IllegalArgumentException("No account for id:"+ id);
            entity.setBalance(entity.getBalance() + amount);
            account = Optional.of(AccountConverter.convert(entity));
//            Thread.sleep(10_000L); // SLEEP!
        } catch (EntityNotFoundException ex) {
            account = Optional.empty();
        }
        return account;
    }

    @Transactional
    @SneakyThrows
    @Override
    public Optional<AccountDto> substractAmount(Long id, Integer amount) {
        if(amount < 0) throw new IllegalArgumentException("Amount cannot be negative");
        Optional<AccountDto> account;
        try {
            AccountEntity entity = accountRepository.getById(id);
            if(entity == null) throw new IllegalArgumentException("No account for id:"+ id);
            if(entity.getBalance() < amount) throw new IllegalArgumentException("Not enough money");
            entity.setBalance(entity.getBalance() - amount);
            account = Optional.of(AccountConverter.convert(entity));
//            Thread.sleep(10_000L); // SLEEP!
        } catch (EntityNotFoundException ex) {
            account = Optional.empty();
        }
        return account;
    }
}
