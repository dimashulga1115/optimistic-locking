package com.project.optimisticlock.account;

public class AccountConverter {
    public static AccountDto convert(AccountEntity entity){
        AccountDto dto = new AccountDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setBalance(entity.getBalance());
        return dto;
    }

    public static AccountEntity convert(AccountDto dto){
        AccountEntity entity = new AccountEntity();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setBalance(dto.getBalance());
        return entity;
    }
}
