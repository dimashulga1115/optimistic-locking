package com.project.optimisticlock.account;

import java.util.List;
import java.util.Optional;

public interface AccountService {
    AccountDto getById(Long id);
    List<AccountDto> getAllAccounts();
    AccountDto createAccount(AccountDto account);
    Optional<AccountDto> addAmount(Long id, Integer amount);
    Optional<AccountDto> substractAmount(Long id, Integer amount);
}
