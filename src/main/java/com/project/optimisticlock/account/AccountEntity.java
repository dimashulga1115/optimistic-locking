package com.project.optimisticlock.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    private String name;
    private Long balance;

    @Version
    private Long version;

    public AccountEntity(Long id, String name, Long balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
}
