# OptimisticLock

- [ ] create RESTful service with Optimistic Lock implementation.
- [ ] implement money income and charge
- [ ] guarantee correct transaction processing in a concurrent environment

## API
### GET localhost:8081/account
получить список счетов
### POST localhost:8081/account
создать новый счет
sample body:
```json
{
    "name": "Account2",
    "balance": 2000
}
```

### GET localhost:8081/account/{id}/increment/{amount}
зачислить сумму `amount` на счет `id`
### GET localhost:8081/account/{id}/decrement/{amount}
списать сумму `amount` со счета `id`

## Теорія
### ACID
- Atomic
  - жодна транзакція не буде виконана частково
  - або виконається повністю, або не виконається зовсім
- Consistent
  - база перебуває в узгодженому стані
- Isolated
  - проміжні зміни не мають бути видимими зовні транзакції для інших транзакцій аж до завершення
- Durable
  - результати завершених транзакцій мають бути збереженими незалежно від збоїв систем
### CAP Theorem
In a distributed computer system, there is always a threshold between:
- Consistency
- Availability
- Partition tolerance

### Transaction isolation levels
#### Levels
- read uncommitted
- read committed
- repeatable read
  - each transaction will read from the consistent snapshot of the database.
  - that's why row, retrieved twice within same transaction 
  - lock-based concurrent control
    - read and write locks
    - no range-locks, so phantom read can occur
  - write skew is possible
    - it's when two writers are allowed to modify same columns 
- serializable 
  - lock-based concurrent control
    - read and write row locks
    - range-locks for a SELECT with a ranged WHERE 
  - sequential execution

#### Problems
- lost update
  - two transaction in parallel, both updating same row, both read initial value
  - classic race condition
  - covered by read uncommitted level
  - does that mean that lost update is not possible? since read uncommitted is the lowest isolation level.
- dirty read
  - one transaction has updated row, second read same row, then first rollbacked 
  - second transaction have changed row before rollback
  - covered by read committed level
- non-repeatable read
  - one transaction select rows twice, second updates rows in between selections
  - row is different for first and second selection
  - covered by repeatable read level
- phantom read
  - one transaction select rows twice, second insert rows in between
  - row amount is different for first and second selection
  - covered by serializable level
